package com.bolsadeideas.springboot.we.app.contoller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/variables")
public class EjemploVariablesRutaController {


	@GetMapping("/string/{texto}/{numero}")
	public String  variables(	@PathVariable String texto,
								@PathVariable int numero,
								Model model) {
		
		model.addAttribute("nombre", "Recibir ruta de la path variables");
		model.addAttribute("resultado","En texto enviado en la pathVariable: " + texto + "y el numero es: " + numero);
		
		return "/variables/ver";
		
		
		
	}
	
	
	
}
