package com.bolsadeideas.springboot.we.app.contoller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class HomeController {

	@GetMapping("/")
	public String home() {
		
		//Cone el redirect recarga la pagina 
		//return "redirect:https://www.google.com/";
		
		//Cone el redirect recarga la pagina 
		//return "redirect:/app/index";
		
		//Cone el forward no recarga la pagina u no sale la url de la redirccion
		return "forward:/app/index";

		
		
		
		
		
	}
	
}
