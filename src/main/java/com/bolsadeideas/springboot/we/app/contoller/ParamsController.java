package com.bolsadeideas.springboot.we.app.contoller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/params")
public class ParamsController {

	
	/*
	 * @RequestMapping(value = "/string", method = RequestMethod.GET) public String
	 * postEmpleado( @RequestParam(name = "nif") int nif,
	 * 
	 * @RequestParam(name = "telefono") int telefono, Model model) {
	 * 
	 * model.addAttribute("nif", "el nif es: " + nif);
	 * model.addAttribute("telefono", "el telefono es: " + telefono);
	 * 
	 * return "params/ver";
	 * 
	 * }
	 * 
	 * 
	 */
	
	
	@GetMapping("/string")
	public String param(@RequestParam(name = "texto", required = false, 
						defaultValue = "algún valor...") String texto, 
						Model model) {
		
		model.addAttribute("resultado", "El texto enviado es: " + texto);
		return "params/ver";
		
	}
	
	@GetMapping("/mix-params")
	public String param(@RequestParam String saludo, 
						@RequestParam int numero, 
						Model model) {
		
		model.addAttribute("resultado", "El texto enviado es: " + saludo + " y el numero es: " + numero);
		return "params/ver";
		
	}

	
	
	
	
	
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index() {
		return "/params/index";
		
		
		
	}
	
	/*
	 * @RequestMapping(value = "/string", method = RequestMethod.GET) public String
	 * postEmpleado( @RequestParam(name = "nif",required=false) String texto, Model
	 * model) {
	 * 
	 * model.addAttribute("nif", "el nif es: " + texto);
	 * 
	 * return "params/ver";
	 * 
	 * 
	 * }
	 * 
	 */
}
