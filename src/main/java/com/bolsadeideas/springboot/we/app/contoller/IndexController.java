package com.bolsadeideas.springboot.we.app.contoller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bolsadeideas.springboot.we.app.model.Usuario;

@Controller
@RequestMapping("/app")
public class IndexController {

	@Autowired
	Usuario usuario;
	List<Usuario> usuarios = new ArrayList<>();

	@Value("${texto.indexController.index.titulo}")
	public String nombre;
	
	@GetMapping(value = { "/index", "/", "/home" })
	public String index(Model model) {
		model.addAttribute("titulo","titulos de css");
		model.addAttribute("hola", nombre);
		return "index";

	}

	@RequestMapping("/perfil")
	public String perfil(Model model) {

		usuario.setNombre("Carlos");
		usuario.setApellido("Garcia");
		usuario.setEmail("carlos@yrtr.vom");

		model.addAttribute("usuario", usuario);
		model.addAttribute("titulo", "Perfil del usuario: ".concat(usuario.getNombre()));

		return "perfil";

	}

	@RequestMapping("/listar")
	public String listar(Model model) {

		usuarios.add(new Usuario("C", "R", "S"));
		usuarios.add(new Usuario("M", "M", "L"));
		usuarios.add(new Usuario("S", "S", "S"));

		model.addAttribute("titulo", "Listado de usuarios");
		model.addAttribute("usuarios", usuarios);

		return "listar";

	}
	
	
	//La anotacion @ModelAttribute sirve para que unmetodo sea comun en cualqueir sitio del modelo
	@ModelAttribute("addUser")
	public List<Usuario> addUser(){
		
		usuarios.add(new Usuario("C", "R", "S"));
		usuarios.add(new Usuario("M", "M", "L"));
		usuarios.add(new Usuario("S", "S", "S","direccion"));

		return usuarios;
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
